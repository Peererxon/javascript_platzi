const anderson = {
    nombre:"Anderson",
    apellido:"Gil",
    edad:19
}

//const cumplaños = persona => persona.edad++
//est metodo podria traer problemas en el futuro ya que hace el objeto sea
// muy maleable y lo modifica pudiendo traer bugs

const cumpleañosInmutable = persona => ({
    ...persona,
    edad:persona.edad++
    //esto crea un nuevo objeto,con los atributos del cual sea pasado
    //por medio de el parametro y modifica este objeto nuevo,dejando
    //el anterior 
})