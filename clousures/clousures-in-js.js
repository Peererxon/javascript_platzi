function CrearVehiclo(finalDeFrase){
    return function(tipoDeCarro){
        //esta funcion recuerda su estado anterior,y al llamarse varias veces no es modificada
        console.log(`${tipoDeCarro} modelo: ${finalDeFrase}`)
    }
}

const motoChebrolet = CrearVehiclo("Chebrolet-xa458"); //Moto modelo: Chebrolet-xa458
const motoSamsung = CrearVehiclo("Samsung-i9200x");  //Moto modelo: Samsung-i9200x
const MotoChavista = CrearVehiclo("Moto");  //Moto Chebrolet-xa458


//en este caso se utiliza la segunda llamada como el principio
motoChebrolet("Moto");
motoSamsung("Moto");
