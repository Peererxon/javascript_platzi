function factorial(n){
    if(!this.cache){
        this.cache = {}
    }
    //Si no existe el objeto cache lo crea (solo una vez)
    debugger
    if(this.cache[n]){
        return this.cache[n];
        //si ya tiene el resultado solo lo regresa
    }

    if(n===1){
        return 1;   
    }
    //esto se llama n veces y se multiplica por si mismo menos 1 hasta llegar a 1
    this.cache[n] = n * factorial(n - 1);
    debugger;
    return this.cache[n];
    }

    //Sirve para ahorrar computo,muy util el aplicaciones o programas grandes